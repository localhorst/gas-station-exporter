#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Author:                     Hendrik Schutter, mail@hendrikschutter.com
    Date of creation:           2022/10/23
    Date of last modification:  2022/10/26
"""

from http.server import BaseHTTPRequestHandler, HTTPServer
import time
import threading
from datetime import datetime
from urllib.parse import urlsplit, parse_qs
from random import randrange
import station_scraper

hostName = "127.0.0.1"
serverPort = 9100
exporter_prefix = "gas_"

stations_ids = (20153, 21907, 183433, 159416, 30856, 16362, 12634)

request_count = 0
scrape_healthy = True
startTime = datetime.now()
station_metrics = list()
mutex = threading.Lock()

class RequestHandler(BaseHTTPRequestHandler):

    def get_metrics(self):
        global request_count
        global station_metrics
        global exporter_prefix
        global mutex
        mutex.acquire()
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write(bytes(exporter_prefix + "expoter_duration_seconds_sum " + str(int((datetime.now() - startTime).total_seconds())) + "\n", "utf-8"))
        self.wfile.write(bytes(exporter_prefix + "exporter_request_count " + str(request_count) + "\n", "utf-8"))
        self.wfile.write(bytes(exporter_prefix + "exporter_scrape_healthy " + str(int(scrape_healthy)) + "\n", "utf-8"))
        
        for metric in station_metrics:
            #print(metric)
            self.wfile.write(bytes(exporter_prefix + metric + "\n", "utf-8"))

        mutex.release()
        
    def do_GET(self):
        global request_count
        request_count = request_count + 1
        print("Request: " + self.path)
        if (self.path.startswith("/metrics")): 
            self.get_metrics()
        else:
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write(bytes("<html>", "utf-8"))
            self.wfile.write(bytes("<head><title>gas station exporter</title></head>", "utf-8"))
            self.wfile.write(bytes("<body>", "utf-8"))
            self.wfile.write(bytes('<h1>gas station exporter based on data from <a href="https://www.clever-tanken.de/">https://www.clever-tanken.de/</a></h1>', "utf-8"))
            self.wfile.write(bytes('<p><a href="/metrics">Metrics</a></p>', "utf-8"))
            self.wfile.write(bytes('<p>obtain station id from <a href="https://www.clever-tanken.de/tankstelle_details/3569">https://www.clever-tanken.de/tankstelle_details/3569</a></p>', "utf-8"))
            self.wfile.write(bytes("</body>", "utf-8"))
            self.wfile.write(bytes("</html>", "utf-8"))


def update_metrics():
    while True:
        print("Scrape")
        global station_metrics
        global mutex
        global scrape_healthy
        mutex.acquire()
        scrape_healthy = True
        station_metrics.clear()

        for station_id in stations_ids:
            try: 
                station_data = station_scraper.scrape_station(station_id)
                #print(station_data)
                for fuel in station_data['fuels']:
                    #print(fuel)
                    station_metrics.append(station_data['station_metric_basename'] + "_" + fuel['name'] + " " + str(fuel['price']))
            except Exception as ex:
                print("scrape error: " + str(ex))
                scrape_healthy = False
                pass
        mutex.release()
        time.sleep(300)


def main():
    print("start")

    webServer = HTTPServer((hostName, serverPort), RequestHandler)

    print("Server started http://%s:%s" % (hostName, serverPort))

    update_metrics_thread = threading.Thread(target=update_metrics, args=())
    update_metrics_thread.start()

    try:
        webServer.serve_forever()
    except KeyboardInterrupt:
        pass

    webServer.server_close()
    print("Server stopped.")
    update_metrics_thread.join()

if __name__ == "__main__":
    main()
