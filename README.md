# gas-station-exporter

Scrapes gas prices from [clever-tanken.de](https://clever-tanken.de) and export them as prometheus metrics

## Requirements

`pip install lxml beautifulsoup4 unidecode`

## Config 

Set your station IDs in list in `gas-stations-exporter.py`.

**Hint** Don´t scrape with higher intervalls due to blocking based on requests!

## Usage 

to start: `python3 gas-station-exporter.py`

to get metrics: `curl http://127.0.0.1:9100/metrics`

Example metrics:

```
gas_expoter_duration_seconds_sum 53
gas_exporter_request_count 1
gas_bft_Hauptstr91_77839_Lichtenau_Diesel 2.129
gas_bft_Hauptstr91_77839_Lichtenau_SuperE10 1.969
gas_bft_Hauptstr91_77839_Lichtenau_SuperE5 1.999
gas_bft_Hauptstr91_77839_Lichtenau_SuperPlus 2.079
gas_TotalEnergies_AmRheinubergang_77866_Freistett_Diesel 2.039
gas_TotalEnergies_AmRheinubergang_77866_Freistett_SuperE10 1.789
gas_TotalEnergies_AmRheinubergang_77866_Freistett_SuperE5 1.849
gas_TotalEnergies_AmRheinubergang_77866_Freistett_ExcelliumSuperPlus 2.019
gas_TotalEnergies_AmRheinubergang_77866_Freistett_LKWDiesel 2.039
gas_TotalEnergies_AmRheinubergang_77866_Freistett_AdBluePKW 1.849
gas_TotalEnergies_AmRheinubergang_77866_Freistett_AdBlueLKW 1.799
gas_Sonstige_Hauptstr_77866_Rheinau_Diesel 2.029
gas_Sonstige_Hauptstr_77866_Rheinau_SuperE10 1.779
gas_Sonstige_Hauptstr_77866_Rheinau_SuperE5 1.839
gas_bft_Hauptstr2_77871_Renchen_Diesel 2.069
gas_bft_Hauptstr2_77871_Renchen_SuperE10 1.779
gas_bft_Hauptstr2_77871_Renchen_SuperE5 1.839
gas_AVIA_Bahnhofstr1_72275_Alpirsbach_Diesel 2.079
gas_AVIA_Bahnhofstr1_72275_Alpirsbach_SuperE10 1.869
gas_AVIA_Bahnhofstr1_72275_Alpirsbach_SuperE5 1.929
gas_AVIA_Bahnhofstr1_72275_Alpirsbach_SuperPlus 2.009
gas_bft_FreudenstadterStr75_72275_Alpirsbach_Diesel 2.069
gas_bft_FreudenstadterStr75_72275_Alpirsbach_SuperE10 1.899
gas_bft_FreudenstadterStr75_72275_Alpirsbach_SuperE5 1.919
gas_AVIA_Hauptstr73_77761_Schiltach_Diesel 2.079
gas_AVIA_Hauptstr73_77761_Schiltach_SuperE10 1.869
gas_AVIA_Hauptstr73_77761_Schiltach_SuperE5 1.929
gas_AVIA_Hauptstr73_77761_Schiltach_SuperPlus 2.009
gas_AVIA_Hauptstr73_77761_Schiltach_LPG 1.099
```

## Permanent Install


`cd /usr/bin/`

`git clone https://git.mosad.xyz/localhorst/gas-station-exporter.git`

`cd gas-station-exporter.git`

Change host and port in `gas-station-exporter.py`

`mv gas-station-exporter.service /usr/lib/systemd/system/`

`systemctl daemon-reload`

`systemctl enable --now /usr/lib/systemd/system/gas-station-exporter.service`

`systemctl status gas-station-exporter.service`
